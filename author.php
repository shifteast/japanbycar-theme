<?php get_header(); ?>

  <?php $author = get_user_by( 'slug', get_query_var( 'author_name' ) ); ?>
  <div class="author-cover-image">
    <div class="background" style="background-image: url(' <?= get_the_author_meta( 'cover_photo', $author->ID ); ?> ')"></div>
  </div>

  <div class="author-card">
    <div class="author-portrait" style="background-image: url(' <?= get_avatar_url($author->ID,array("size"=>260)); ?> ')"></div>
    <div class="author-name">
      <?= $author->display_name; ?>
    </div>
    <div class="author-bio">
      <?= get_the_author_meta( 'description', $author->ID ); ?> 
    </div>
  </div>

  <?php require get_template_directory() . '/components/search-field.php'; ?>

  <div class="itineraries-list">

    <?php
    $the_query = new WP_Query( 
      array(
        'author' => $author->ID,
        'post_type' => 'itinerary',
        'posts_per_page' => '-1'
      )
    );
    if ( $the_query->have_posts() ) {
      $count = 0;
      while ( $the_query->have_posts() ) : $the_query->the_post(); 
        if($count != 0) {
          ?>
          <div class="divider"></div>
          <?php
        }
        $count++;
        require get_template_directory() . '/components/itinerary-preview-container.php';
      endwhile;
    }
    wp_reset_postdata();
    ?>

  </div>

<?php get_footer(); ?>