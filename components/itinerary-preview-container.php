<div class="itinerary-preview-container">
  <div class="left-container">
    <div class="itinerary-map">
      <div class="background" style="background-image: url('https://cdn-images-1.medium.com/max/1600/0*yPSQlTHRvLaIVBcG.jpg');"></div>
    </div>
    <?php
    if( count( get_post_gallery_ids() ) == 0 ) {
    ?>
    <div class="itinerary-gallery in-front">
      <div class="gallery-item active">
        <div class="background" style="background-image: url('<?= get_the_post_thumbnail_url(get_the_ID(),'large'); ?>')"></div>
      </div>
    </div>
    <?php
    } else {
    ?>
    <div class="itinerary-gallery in-front">
      <?php
      $image_count = 0;
      foreach( get_post_gallery_ids() as $image_id) {
        $image_count == 0 ? $is_active = 'active' : $is_active = '';
        $image_count++;
        $image_url = wp_get_attachment_url( $image_id, 'large' );
        ?>
        <div class="gallery-item <?= $is_active; ?>">
          <div class="background" style="background-image: url('<?= $image_url; ?>')"></div>
        </div>
        <?php
      }
      ?>
      <div class="dots-nav">
        <?php
        $image_count = 0;
        foreach( get_post_gallery_ids() as $image_id) {
          $image_count == 0 ? $is_active = 'active' : $is_active = '';
          $image_count++;
          ?>
          <div class="dot <?= $is_active; ?>"></div>
          <?php
        }
        ?>
      </div>
      <div class="nav-prev-btn"><i class="icon ion-md-arrow-dropleft"></i></div>
      <div class="nav-next-btn"><i class="icon ion-md-arrow-dropright"></i></div>
    </div>
    <?php
    }
    ?>
  </div>
  <div class="right-container">
    <?php generate_itinerary_info_tags($post); ?>
    <a class="itinerary-title" href="<?= get_permalink(); ?>"><?= get_the_title(); ?></a>
    <p class="itinerary-excerpt">
      <?= get_the_excerpt(); ?>
    </p>
    <a class="btn" label="Discover Itinerary" href="<?= get_permalink(); ?>">Discover Itinerary</a>
  </div>
</div>