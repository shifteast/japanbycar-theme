<div class="prefecture-badge-container">
  <a href="<?= get_term_link($prefecture->term_id); ?>" class="prefecture-badge">
    <div class="background" style="background-image: url('<?php echo get_template_directory_uri() . '/assets/images/prefectures/'. strtolower($prefecture->name) .'.jpg'; ?>')"></div>
    <div class="content">
      <?= $prefecture->name ?>
    </div>
  </a>
</div>