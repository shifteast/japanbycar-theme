<?php
$the_query = new WP_Query( 
  array(
    'post_type' => 'itinerary',
    'posts_per_page' => '4'
  )
);
wp_reset_postdata();
?>

<div class="itineraries-slider">
  <div class="slides">
    <?php
    if ( $the_query->have_posts() ) {
      $count = 0;
      while ( $the_query->have_posts() ) : $the_query->the_post(); 
        $count == 0 ? $is_active = 'active' : $is_active = '';
        $count++;
        ?>
        <div class="slide <?= $is_active; ?>">
          <div class="background" style="background-image: url('<?= get_the_post_thumbnail_url(get_the_ID(),'large-4x'); ?>')"></div>
          <div class="filter"></div>
          <div class="content">
            <?php generate_itinerary_info_tags($post); ?>
            <a href='<?= get_the_permalink(); ?>'><?= get_the_title(); ?></a>
          </div>
        </div>
        <?php
      endwhile;
    }
    ?>
  </div>
  <div class="slider-nav">
    <div class="gradient-bg"></div>
    <?php
    if ( $the_query->have_posts() ) {
      $count = 0;
      while ( $the_query->have_posts() ) : $the_query->the_post(); 
        $count == 0 ? $is_active = 'active' : $is_active = '';
        $count++;
        ?>
        <div class="slider-nav-item <?= $is_active; ?>" data-href="<?= get_the_permalink(); ?>">
          <div class="itinerary-title">
            <?= get_the_title(); ?>
          </div>
        </div>
        <?php
      endwhile;
    }
    ?>
  </div>
</div>