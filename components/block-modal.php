<div id="block-modal-container">
  <div class="loading-a-block">
    <div class="loader">Loading...</div>
    Loading Information
  </div>
  <div class="block-card-container">
    <div class="block-card">
      <div class="close-block-modal">
        <i class="icon ion-md-close"></i>
      </div>
      <div class="block-card-gallery">
        <!-- Will be filled by js -->
      </div>
      <div class="block-card-body">
        <h3><!-- Will be filled by js --></h3>
        <p><!-- Will be filled by js --></p>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function($){

  window.populateModal = function(id, callback) {
    var $block_card = $('#block-modal-container .block-card');
    $.ajax({
        type: "GET",
        url: "https://api.shifteast.com/api/blocks/"+id,
        success: function(data) {
          // Title
          $block_card.find('h3').html(data.en.name);
          // Description
          if(data.en.long_description) {
            $block_card.find('p').html(data.en.long_description);
          } else {
            $block_card.find('p').html(data.en.description);
          }

          // Gallery Items markup
          var slides_markup = "";
          data.images.forEach(function(image,index) {
            var active = '';
            index == 0 ? active = 'active' : active = '' 
            slides_markup += "<div class='gallery-item "+active+"' style='background-image: url(https://ik.imagekit.io/shifteast/tr:w-600/" + image._id + "." + image.ext+")'></div>";
          });
          $block_card.find('.block-card-gallery').html(slides_markup);
          // Dots markup
          var dots_markup = "<div class='dots-nav'>";
          data.images.forEach(function(image,index) {
            var active = '';
            index == 0 ? active = 'active' : active = '' 
            dots_markup += "<div class='dot "+active+"'></div>";
          });
          dots_markup += "</div>";
          $block_card.find('.block-card-gallery').append(dots_markup);
          // Prev/Next btn markup
          var prev_nav_btn_markup = "<div class='nav-prev-btn'><i class='icon ion-md-arrow-dropleft'></i></div>";
          $block_card.find('.block-card-gallery').append(prev_nav_btn_markup);
          var next_nav_btn_markup = "<div class='nav-next-btn'><i class='icon ion-md-arrow-dropright'></i></div>"
          $block_card.find('.block-card-gallery').append(next_nav_btn_markup);

          // Gallery JS Code
          $('#block-modal-container .block-card-container .block-card-gallery .nav-prev-btn').on('click', function () {
            var active_dot = $(this).parent().find('.dot.active');
            var current_index = $(this).parent().find('.dot').index(active_dot);
            if(current_index - 1 < 0) {
              var prev_index = $(this).parent().find('.dot').length - 1;
            } else {
              var prev_index = current_index - 1;
            }
            $(this).parent().find('.dot').removeClass('active');
            $(this).parent().parent().find('.gallery-item').removeClass('active');
            $(this).parent().parent().find('.dot').eq(prev_index).addClass('active');
            $(this).parent().parent().find('.gallery-item').eq(prev_index).addClass('active');
          });

          $('#block-modal-container .block-card-container .block-card-gallery .nav-next-btn').on('click', function () {
            var active_dot = $(this).parent().find('.dot.active');
            var current_index = $(this).parent().find('.dot').index(active_dot);
            if (current_index + 1 > $(this).parent().find('.dot').length - 1) {
              var next_index = 0;
            } else {
              var next_index = current_index + 1;
            }
            $(this).parent().find('.dot').removeClass('active');
            $(this).parent().parent().find('.gallery-item').removeClass('active');
            $(this).parent().parent().find('.dot').eq(next_index).addClass('active');
            $(this).parent().parent().find('.gallery-item').eq(next_index).addClass('active');
          });

          callback();
        }
    });
  }

  window.showModal = function(id) {
    $('#block-modal-container').addClass('visible');
    $('.loading-a-block').show();
    $('.block-card-container').hide();
    populateModal(id, function() {
      $('.loading-a-block').hide();
      $('.block-card-container').show();
    });
  }

  window.closeModal = function() {
    $('#block-modal-container').removeClass('visible');
  }

  $('.close-block-modal').on('click', function() {
    closeModal()
  });

  $('#block-modal-container').on('click', function(e) {
    closeModal()
  }).children().click(function(e) {
    return false;
  });

  $(document).on('keyup', function(e) {
    if(e.keyCode == 27) {
      closeModal()
    }
  })
}); 
</script>