<div class="search-field-wrap">
  <form class="search-field" role="search" method="get" action="<?= get_home_url(); ?>">
    <i class="icon ion-md-search"></i>
    <input type="text" name="s" placeholder="Search ...">
  </form>
</div>