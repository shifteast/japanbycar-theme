<?php

function generate_itinerary_info_tags($post) {
  if(get_post_meta( $post->ID, 'season' ) && get_post_meta( $post->ID, 'season' )[0] != 'any') { ?>
    <div class="season-tag <?= get_post_meta( $post->ID, 'season' )[0]; ?>"><?= get_post_meta( $post->ID, 'season' )[0]; ?></div>
  <?php }
  if(get_post_meta( $post->ID, 'duration' )) { ?>
    <div class="duration-tag spring"><?= get_post_meta( $post->ID, 'duration' )[0]; ?> Days</div>
  <?php }
}

function generate_itinerary_preview($post) {
  if(get_post_meta( $post->ID, 'stopovers' )) { ?>
    <div class="itinerary-preview">
      <?php
      $stopovers = explode(',', get_post_meta( $post->ID, 'stopovers' )[0]);
      $count = 0;
      foreach($stopovers as $stopover_string) {
        $stopover = explode('||', $stopover_string);
        $count == 0 || $count == count($stopovers) - 1 ? $dot_type = 'big-dot' : $dot_type = 'dot';
        ?>
        <div class="<?= $dot_type; ?>" style="left: <?= $stopover[1] ?>%;">
          <div class="label"><?= $stopover[0] ?></div>
        </div>
        <?php
        $count++;
      }
      ?>
    </div>
  <?php }
}

function retrieve_itinerary_prefectures($post) {
  if(get_post_meta( $post->ID, 'prefectures' )) {
    $prefectures = get_post_meta( $post->ID, 'prefectures' );
    $prefectures = str_replace( ' ', '', $prefectures );
    $prefectures = explode( ',', $prefectures[0] );
  }
}