<?php 
get_header(); 
// let's get the term name !
$term_name = get_term_by('id', get_queried_object()->term_id, 'prefecture')->name;
?>

  <div class="japan-map-hero">
    <div class="prefecture-background-image" style="background-image: url('<?php echo get_template_directory_uri() . '/assets/images/prefectures/'. strtolower($term_name) .'.jpg'; ?>')"></div>
    <div class="background-filter"></div>
    <div class="content">
      <div class="japan-map">
        <?php echo file_get_contents( get_template_directory_uri() . '/assets/svg/jp.svg' ); ?>
      </div>
      <div class="prefecture-name">
        <?= $term_name; ?>
      </div>
    </div>
  </div>

  <div class="itineraries-list margintop">

    <?php
    if ( have_posts() ) {
      ?>
      <?php
      $count = 0;
      while ( have_posts() ) : the_post(); 
        if($count != 0) {
          ?>
          <div class="divider"></div>
          <?php
        }
        $count++;
        require get_template_directory() . '/components/itinerary-preview-container.php';
      endwhile;
    } else {
      ?>
      <h1 class="search-query">Oh no ! We couldn't find anything for <i><?= get_search_query(); ?></i></h1>
      <?php
    }
    ?>

  </div>

  <script type="text/javascript">
    jQuery('.japan-svg-map').find('path[name="<?= $term_name; ?>"]').attr('fill', '#1B9CFC').attr('fill-opacity', '1').attr('stroke', '#1B9CFC');
    console.log(jQuery);
  </script>

<?php get_footer(); ?>