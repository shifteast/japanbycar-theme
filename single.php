<?php get_header(); ?>

  <?php
  while ( have_posts() ) :
    the_post(); ?>
    <div class="itinerary-header">
      <div class="background" style="background-image: url('<?= get_the_post_thumbnail_url(get_the_ID(),'large-4x'); ?>')"></div>
      <div class="filter"></div>
      <div class="content">
        <?php generate_itinerary_info_tags($post); ?>
        <h1><?= get_the_title(); ?></h1>
        <div class="author-card">
          <a class="author-portrait" href="<?= get_author_posts_url( get_the_author_meta('ID') ); ?>" style="background-image: url('<?= get_avatar_url( get_the_author_meta('ID') ) ?>')"></a>
          <div class="author-infos">
            Written By
            <div class="author-name">
              <a href="<?= get_author_posts_url( get_the_author_meta('ID') ); ?>"><?= get_the_author(); ?></a>
            </div>
          </div>
        </div>
      </div>
      <div class="itinerary-preview-container">
        <div class="gradient-bg"></div>
        <?php generate_itinerary_preview($post); ?>
      </div>
    </div>

    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  <?php
  endwhile;
  ?>

<?php get_footer(); ?>