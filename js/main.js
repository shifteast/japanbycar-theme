jQuery(document).ready(function ($) {

  // Add dropdown icon to menu items having a submenu
  $('.site-navigation ul li.menu-item-has-children').each(function () {
    $(this).children('a').append('<span class="submenu-indicator">&#9660;</span>');
  });

  // Slider JS
  var $itinerariesSlider = $('.itineraries-slider');

  function changeActiveSlide(index) {
    $itinerariesSlider.find('.slide').removeClass('active');
    $itinerariesSlider.find('.slider-nav-item').removeClass('active');
    $itinerariesSlider.find('.slide').eq(index).addClass('active');
    $itinerariesSlider.find('.slider-nav-item').eq(index).addClass('active');
  }

  window.itinerariesSliderInterval = setInterval(function () {
    var index = $('.slider-nav-item.active').index();
    if (index + 1 >= $('.slider-nav-item').length) {
      index = 0;
    } else {
      index++;
    }
    changeActiveSlide(index);
  }, 10000);

  $('.slider-nav-item').on('click', function () {
    // if($(this).hasClass('active')) {
      window.location.href = $(this).attr('data-href');
    // }
    /*
    var index = $('.slider-nav-item').index(this);
    changeActiveSlide(index);
    clearInterval(window.itinerariesSliderInterval);
    window.itinerariesSliderInterval = setInterval(function () {
      var index = $('.slider-nav-item.active').index();
      if (index + 1 >= $('.slider-nav-item').length) {
        index = 0;
      } else {
        index++;
      }
      changeActiveSlide(index);
    }, 10000);
    */
  });



  // Itinerary Preview Gallery
  $('.itinerary-preview-container .left-container .itinerary-gallery .dot').on('click', function () {
    $(this).parent().find('.dot').removeClass('active');
    $(this).parent().parent().find('.gallery-item').removeClass('active');
    $(this).addClass('active');
    var index = $(this).parent().find('.dot').index(this);
    $(this).parent().parent().find('.gallery-item').eq(index).addClass('active');
  });

  $('.itinerary-preview-container .left-container .itinerary-gallery .nav-prev-btn').on('click', function () {
    var active_dot = $(this).parent().find('.dot.active');
    var current_index = $(this).parent().find('.dot').index(active_dot);
    if(current_index - 1 < 0) {
      var prev_index = $(this).parent().find('.dot').length - 1;
    } else {
      var prev_index = current_index - 1;
    }
    $(this).parent().find('.dot').removeClass('active');
    $(this).parent().parent().find('.gallery-item').removeClass('active');
    $(this).parent().parent().find('.dot').eq(prev_index).addClass('active');
    $(this).parent().parent().find('.gallery-item').eq(prev_index).addClass('active');
  });

  $('.itinerary-preview-container .left-container .itinerary-gallery .nav-next-btn').on('click', function () {
    var active_dot = $(this).parent().find('.dot.active');
    var current_index = $(this).parent().find('.dot').index(active_dot);
    if (current_index + 1 > $(this).parent().find('.dot').length - 1) {
      var next_index = 0;
    } else {
      var next_index = current_index + 1;
    }
    $(this).parent().find('.dot').removeClass('active');
    $(this).parent().parent().find('.gallery-item').removeClass('active');
    $(this).parent().parent().find('.dot').eq(next_index).addClass('active');
    $(this).parent().parent().find('.gallery-item').eq(next_index).addClass('active');
  });

  // Itinerary Header Parralax Effect
  if ($('.itinerary-header').length > -1) {
    $(window).on('scroll', function () {
      var scrollTop = $(this).scrollTop();
      $('.itinerary-header .background').css('transform', 'translate3d(0,' + scrollTop / 4 + 'px,0)')
    })
  }

  // Author Header Parralax Effect
  if ($('.author-cover-image').length > -1) {
    $(window).on('scroll', function () {
      var scrollTop = $(this).scrollTop();
      $('.author-cover-image .background').css('transform', 'translate3d(0,' + scrollTop / 4 + 'px,0)')
    })
  }

  // Itinerary Preview
  if($('.itinerary-preview').length > -1) {
    $('.big-dot').each(function () {
      $(this).find('.label').css('margin-left', -$(this).find('.label').outerWidth() / 2 + 10 + 'px');
    });
    $('.dot').each(function() {
      $(this).find('.label').css('margin-left', -$(this).find('.label').outerWidth()/2 + 5 + 'px');
    });
  }

  window.toggleNavSidebar = function() {
    $('body').toggleClass('sidebar-visible');
  }

});