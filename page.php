<?php get_header(); ?>

  <?php
  while ( have_posts() ) :
    the_post(); ?>
    <div class="itinerary-header smaller">
      <div class="background" style="background-image: url('<?= get_the_post_thumbnail_url(get_the_ID(),'large-4x'); ?>')"></div>
      <div class="filter"></div>
      <div class="content">
        <h1><?= get_the_title(); ?></h1>
      </div>
    </div>

    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  <?php
  endwhile;
  ?>

<?php get_footer(); ?>