<html lang="en">
<head>
  <meta charset="utf-8">
  <link href="https://unpkg.com/ionicons@4.4.6/dist/css/ionicons.min.css" rel="stylesheet">
  <?php wp_head(); ?>
  <?php
  $whitelist = array(
      '127.0.0.1',
      '::1'
  );

  if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
    ?>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-135151307-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-135151307-1');
    </script>
    <?php
  }
  ?>

</head>

<body <?php body_class(); ?>>

  <div class="nav-sidebar">
    <div class="close-nav-sidebar" onClick="toggleNavSidebar()">Close</div>
    <?php
    wp_nav_menu( array(
      'theme_location' => 'menu-1',
      'menu_id'        => 'top-menu',
    ) );
    ?> 
  </div>

  <div class="site-header">
    <div class="top-bar">
      <div class="site-branding">
        <?php
        if ( has_custom_logo() ) {
        ?>
          <div class="site-logo">
            <a href="<?= get_home_url(); ?>">
              <?= get_custom_logo(); ?>
            </a>
          </div>
        <?php
        } else {
          echo "<a href='".get_home_url()."'>Japan By Car</a>";
        }
        ?>
      </div>
      <div class="site-navigation">
        <?php
        wp_nav_menu( array(
          'theme_location' => 'menu-1',
          'menu_id'        => 'top-menu',
        ) );
        ?>
      </div>
      <div class="open-nav-sidebar" onClick="toggleNavSidebar()">
        Menu
      </div>
    </div>
  </div>