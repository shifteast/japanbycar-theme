<?php
/*
Template Name: Prefectures Page
*/
get_header();
?>

  <div class="default-page-hero">
    <h1 class="page-title">Search by Prefecture</h1>
  </div>

  <div class="japanese-prefectures-list-container">
    <?php
    $terms = get_terms('prefecture', array(
      'hide_empty' => true,
    ));
    foreach($terms as $prefecture) {
      require get_template_directory() . '/components/prefecture-badge.php';
    }
    ?>
  </div>

<?php
get_footer();