<?php

if ( ! function_exists( 'japanbycar_setup' ) ) :

	function japanbycar_setup() {

		load_theme_textdomain( 'japanbycar', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'align-wide' );

		add_theme_support( 'editor-color-palette',
						array(
								'name' => 'dark blue',
								'color' => '#1767ef',
						),
						array(
								'name' => 'light gray',
								'color' => '#eee',
						),
						array(
								'name' => 'dark gray',
								'color' => '#444',
						)
				);


		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
            'menu-1' => esc_html__( 'Top Menu', 'japanbycar' )
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'japanbycar_setup' );

/**
 * Enqueue scripts and styles.
 */
function japanbycar_scripts() {
    $ver_num = mt_rand();

	wp_enqueue_style( 'japanbycar-style', get_stylesheet_uri(), array(), $ver_num );

	wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), $ver_num, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'japanbycar_scripts' );

/**
 * Define new image size
 */
add_image_size( 'large-2x', 1980, 1020 );
add_image_size( 'large-4x', 3840, 2160 );

/**
 * Create Itinerary Post Type
 */
function create_itinerary_post_type() {
  register_post_type( 'itinerary',
    array(
      'labels' => array(
        'name' => __( 'Itineraries' ),
        'singular_name' => __( 'Itinerary' )
      ),
      'public' => true,
      'has_archive' => true,
      'menu_position' => 5,
      'menu_icon' => 'dashicons-location-alt',
      'supports' => ['title', 'editor', 'excerpt', 'author', 'revisions', 'thumbnail'],
      'show_in_rest' => true,
      'register_meta_box_cb' => 'add_itinerary_cpt_metaboxes',
    )
  );
}
add_action( 'init', 'create_itinerary_post_type' );

/**
 * Itineraries metaboxes
 */
function add_itinerary_cpt_metaboxes( ) {
	// Itinerary Season
	add_meta_box(
		'itinerary_stopovers',
		'Itinerary Stopovers',
		'itinerary_stopovers_generator',
		'itinerary',
		'side',
		'high'
	);
	// Itinerary Duration
	add_meta_box(
		'itinerary_duration',
		'Itinerary Duration',
		'itinerary_duration_generator',
		'itinerary',
		'side',
		'high'
	);
	// Itinerary Season
	add_meta_box(
		'itinerary_season',
		'Itinerary Season',
		'itinerary_season_generator',
		'itinerary',
		'side',
		'high'
	);
}

function itinerary_stopovers_generator() {
	global $post;
	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'event_fields' );
	// Get the location data if it's already been entered
	$stopovers = get_post_meta( $post->ID, 'stopovers', true );
	// Output the field
	echo '<label>Itinerary Stopovers</label>';
	echo '<textarea type="number" name="stopovers" placeholder="Tokyo || 0, Nagoya || 40, Kyoto || 100" class="widefat">';
	echo esc_textarea( $stopovers );
	echo '</textarea>';
}

function itinerary_duration_generator() {
	global $post;
	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'event_fields' );
	// Get the location data if it's already been entered
	$duration = get_post_meta( $post->ID, 'duration', true );
	$duration == '' ? $duration = '0' : null;
	// Output the field
	echo '<label>Duration of the itinerary, in days.</label>';
	echo '<input type="number" name="duration" value="' . esc_textarea( $duration )  . '" class="widefat">';
}

function is_selected_season($season, $value) {
	if($season == $value) {
		return "selected";
	}
}

function itinerary_season_generator() {
	global $post;
	// Nonce field to validate form request came from current site
	wp_nonce_field( basename( __FILE__ ), 'event_fields' );
	// Get the location data if it's already been entered
	$season = get_post_meta( $post->ID, 'season', true );
	$season == '' ? $season = 'any' : null;
	$options = [
		array(
			'value' => 'any',
			'display' => 'Anytime'
		),
		array(
			'value' => 'spring',
			'display' => 'Spring'
		),
		array(
			'value' => 'summer',
			'display' => 'Summer'
		),
		array(
			'value' => 'autumn',
			'display' => 'Autumn'
		),
		array(
			'value' => 'winter',
			'display' => 'Winter'
		),
	];
	// Output the field
	echo '<label>Best season for this trip.</label>';
	echo '<select name="season" class="widefat" autocomplete="off">';
		foreach($options as $option) {
			if($season == $option['value']) {
				echo sprintf('<option selected value="%s">%s</option>', $option['value'],$option['display']);
			} else {
				echo sprintf('<option value="%s">%s</option>', $option['value'],$option['display']);
			}
		}
	echo '</select>';
}

/**
 * Save the metabox data
 */
function wpt_save_events_meta( $post_id, $post ) {
	// Return if the user doesn't have edit permissions.
	if ( ! current_user_can( 'edit_post', $post_id ) ) {
		return $post_id;
	}

	// Verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times.
	if ( ! isset( $_POST['duration'] ) || ! isset( $_POST['season'] ) || ! wp_verify_nonce( $_POST['event_fields'], basename(__FILE__) ) ) {
		return $post_id;
	}
	// Now that we're authenticated, time to save the data.
	// This sanitizes the data from the field and saves it into an array $events_meta.
	$events_meta['stopovers'] = esc_textarea( $_POST['stopovers'] );
	$events_meta['duration'] = esc_textarea( $_POST['duration'] );
	$events_meta['season'] = esc_textarea( $_POST['season'] );

	// Cycle through the $events_meta array.
	// Note, in this example we just have one item, but this is helpful if you have multiple.
	foreach ( $events_meta as $key => $value ) :
		// Don't store custom data twice
		if ( 'revision' === $post->post_type ) {
			return;
		}
		if ( get_post_meta( $post_id, $key, false ) ) {
			// If the custom field already has a value, update it.
			update_post_meta( $post_id, $key, $value );
		} else {
			// If the custom field doesn't have a value, add it.
			add_post_meta( $post_id, $key, $value);
		}
		if ( ! $value ) {
			// Delete the meta key if there's no value
			delete_post_meta( $post_id, $key );
		}
	endforeach;
}
add_action( 'save_post', 'wpt_save_events_meta', 1, 2 );

/**
 * User Metabox
 */
function my_show_extra_profile_fields( $user ) { ?>

	<h3>Extra profile information</h3>

	<table class="form-table">

		<tr>
			<th><label for="twitter">Cover Photo</label></th>
			<td>
				<input type="text" name="cover_photo" id="cover_photo" value="<?php echo esc_attr( get_the_author_meta( 'cover_photo', $user->ID ) ); ?>" class="regular-text" /><br />
				<span class="description">Please enter your cover photo image URL.</span>
			</td>
		</tr>
	</table>
<?php 
}
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) )
        return false;

    /* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
    update_usermeta( $user_id, 'cover_photo', $_POST['cover_photo'] );
}
add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

/**
 * Add featured Galleries to itineraries
 */
function add_featured_galleries_to_itineraries( $postTypes ) {

    array_push($postTypes, 'itinerary' );

    return $postTypes;

}

add_filter('fg_post_types', 'add_featured_galleries_to_itineraries' );

/**
 * Add Prefectures Taxonomy to Itineraries
 */
function add_prefecture_taxonomy() {
	// create a new taxonomy
	register_taxonomy(
		'prefecture',
		'itinerary',
		array(  
				'public' => true,
				'show_in_rest' => true,
				'hierarchical' => true,  
				'label' => 'Prefectures',  //Display name
				'sort' => false,
				'labels' => array(
					'add_new_item' => __('Add a new Prefecture')
				),
				'query_var' => true,
				'rewrite' => array(
						'slug' => 'prefecture', // This controls the base slug that will display before each term
						'with_front' => false // Don't display the category base before 
				)
		) 
	);
}
add_action( 'init', 'add_prefecture_taxonomy' );

/**
 * Load PHP Helpers
 */
require get_template_directory() . '/inc/php-helpers.php';

function searchfilter($query) {
 
    if ($query->is_search && !is_admin() ) {
        $query->set('post_type',array('itinerary'));
    }
 
return $query;
}
 
add_filter('pre_get_posts','searchfilter');

// [block id="a-block-id"]
function bartag_func( $atts, $content ) {
	$a = shortcode_atts( array(
		'id' => false
	), $atts );

	return "<span class='shifteast-block-trigger-link' rel='block' data-block-id='{$a['id']}' onClick=\"window.showModal('{$a['id']}')\">{$content}</span>";
}
add_shortcode( 'block', 'bartag_func' );