<?php get_header(); ?>

  <?php require get_template_directory() . '/components/search-field.php'; ?>

  <div class="itineraries-list">

    <?php
    if ( have_posts() ) {
      ?>
      <h1 class="search-query">Results for <i><?= get_search_query(); ?></i></h1>
      <div class="divider"></div>
      <?php
      $count = 0;
      while ( have_posts() ) : the_post(); 
        if($count != 0) {
          ?>
          <div class="divider"></div>
          <?php
        }
        $count++;
        require get_template_directory() . '/components/itinerary-preview-container.php';
      endwhile;
    } else {
      ?>
      <h1 class="search-query">Oh no ! We couldn't find anything for <i><?= get_search_query(); ?></i></h1>
      <?php
    }
    ?>

  </div>

<?php get_footer(); ?>