<?php get_header(); ?>

  <?php require get_template_directory() . '/components/itineraries-slider.php'; ?>

  <?php require get_template_directory() . '/components/search-field.php'; ?>

  <div class="itineraries-list">

    <?php
    $the_query = new WP_Query( 
      array(
        'post_type' => 'itinerary',
        'posts_per_page' => '-1'
      )
    );
    if ( $the_query->have_posts() ) {
      $count = 0;
      while ( $the_query->have_posts() ) : $the_query->the_post(); 
        if($count != 0) {
          ?>
          <div class="divider"></div>
          <?php
        }
        $count++;
        require get_template_directory() . '/components/itinerary-preview-container.php';
      endwhile;
    }
    wp_reset_postdata();
    ?>

  </div>

<?php get_footer(); ?>